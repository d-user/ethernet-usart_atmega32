
#include <avr/io.h>
#include "Init_MCU.h"

#define F_CPU 8000000UL 

void Init_Usart (unsigned long int BAUDRATE){
	UCSRA =0;
	UCSRB = (1<<RXCIE)|(1<<RXEN)|(1<<TXEN);
	UCSRC |= (1<<URSEL)|(1<<UCSZ1)|(1<<UCSZ0);
	UBRRL = (F_CPU/BAUDRATE/16-1); // ��������� �������� ������ �������
	UBRRH = (F_CPU/BAUDRATE/16-1) >> 8;
}

void Init_Port_A (uint8_t ddrA, uint8_t portA){
	DDRA |= ddrA;
	PORTA |= portA;
}

void Init_Port_B (uint8_t ddrB, uint8_t portB){
	DDRB |= ddrB;
	PORTB |= portB;
}
void Init_Port_C (uint8_t ddrC, uint8_t portC){
	DDRC |= ddrC;
	PORTC |= portC;
}
void Init_Port_D (uint8_t ddrD, uint8_t portD){
	DDRD |= ddrD;
	PORTD |= portD;
}

void Init_Timer_Os (unsigned long int time){
	
	TCCR2|=(1<<CS20)|(1<<CS22); // ����������� ��������� ��������, .
	TIMSK|=(1<<TOIE2); //  ��������� ���������� �� ������������ �2.
	TCNT2 = time;
}