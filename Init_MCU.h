
#include <avr/io.h>

void Init_Usart (unsigned long int BAUDRATE);
void Init_Port_A (uint8_t ddrA, uint8_t portA);
void Init_Port_B (uint8_t ddrB, uint8_t portB);
void Init_Port_C (uint8_t ddrC, uint8_t portC);
void Init_Port_D (uint8_t ddrD, uint8_t portD);
void Init_Timer_Os (unsigned long int time);