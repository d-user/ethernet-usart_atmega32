/*
 * Ethernet_usart_atm32.c
 *
 * Created: 15.02.2017 21:16:03
 * Author : ADMIN
 */ 

#define F_CPU 8000000UL
#include <avr/io.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "timeout.h" // defines F_CPU

#include <avr/interrupt.h>
#include <avr/wdt.h>
#include <util/delay.h>
// ethernet interf
#include "ip_arp_udp_tcp.h"
#include "enc28j60.h"
#include "net.h"
#include "ip_config.h"
#include "Init_MCU.h"
#include "usart.h"

#define MAX_LED 2

static uint8_t mymac[6] = {0x54,0x55,0x58,0x10,0x00,0x24};// ����������� MAC-����� ����������
static uint8_t myip[4] = {10,0,0,25};// ����������� IP-����� ����������
#define BUFFER_SIZE 400// ��������� ������ ������ �� ������ ������
static uint8_t buf[BUFFER_SIZE+1];//�������������� ������ ������ �� ������
static uint16_t myport=1200;//���� �� UDP
static uint8_t dstmac[6] = {0xff,0xff,0xff,0xff,0xff,0xff};
static uint8_t dip[4] = {10,0,0,55};

// ���������, ����������� ����� ������ �����������
struct lde{
	unsigned long int LED; // ���� ������� ��������������
	unsigned long int LED_Time; // �������� �� �������� ���������� ����
	uint8_t activity; // ���������: ON/OFF
}Led_[MAX_LED];
	
char usart_data; // ����������� ��� ���������� �������� �� ��������� ������
char data_cmd[70]; // ������ � ������� ���������� �������� ��� ������ �� usart
char data_buff[70];
char *adr_usart;
unsigned char index_ = 0;

// ������� ������� �������� ������� � ������� ��������� ������
void ClearTotalBuff(){
	for (uint8_t index=70; index!=0; index--){
		data_cmd[index] = 0;
		data_buff[index] = 0;
	}
	data_cmd[0]=0;
	usart_data = 0;
	data_buff[0] = 0;
	index_ = 0;
}

// ���������� �� ������ �����

ISR(USART_RXC_vect){
		usart_data = UDR;  // ������ ������
		if (usart_data != 0x08){ // ��������� �� ������ �� "0�08", ���� ��� ������������ � ������ ��������� ������
			adr_usart=&usart_data; 
			data_cmd[index_++]= *adr_usart;
			
		}
		else if (usart_data == 0x08){ // ����� ������ ������ ���������� ������, ���������� 0 �������� ��������� �����
			if (index_ <= 1) index_ = 1; 
			data_cmd[index_--] = 0;
		}
}


// ���������� ����������� �������, �������� 1 ��� � 10 ��
// � ���� ������� ��� �������� ���������� ���������� ������ �����
ISR (TIMER2_OVF_vect){
	for(uint8_t i=0; i<MAX_LED; i++){ // ���������� �� ���� �����������
			Led_[i].LED++; // ����������� ��������, ������� ����� ���������� � ��������� (��������) 
			if (Led_[i].LED == Led_[i].LED_Time){ // ���� ��� �����
				Led_[i].activity = 0; // ���������� � ��� ���� ��������� ���������
				switch(i){
					case 0:PORTD &= ~(1<<PD2);break; // �������� ��������������� ��������� � ����� ���
					case 1:PORTD &= ~(1<<PD3);break;
				}
				
			}
			else if(Led_[i].activity == 0){ // ���� � ���� ���������� �������, ��� ��������� ��� ��������
				Led_[i].LED = 0; // �� �������� ���
			}
		}
		TCNT2 = 250; // ����� ���������� ��������� �������� ��������, � �������� �� ����� �������
		PORTD &=~(1<<PD4);
	}

// �������, ����������� ��� ����������� , � ��������� �������� ��������� � ������ � ������� ����� ��������� �������

uint8_t led_0_status(uint8_t status, unsigned long int time){
	PORTD |= (1<<PD2);
	Led_[0].activity = status;
	Led_[0].LED_Time = time;
	return 0;
}

uint8_t led_1_status(uint8_t status, unsigned long int time){
	PORTD |= (1<<PD3);
	Led_[1].activity = status;
	Led_[1].LED_Time = time;
	return 0;
}

int main(void)
{
   Init_Port_B(0xFF, 0); // ������������� ������
   Init_Port_D(0xFC, 0);
   Init_Usart(9600); // ���������� �������� ������ USART
   Init_Timer_Os(250); // �������������� ������� � ��������� ���
  // USART_TransmitStr("Hello!\r");
   uint16_t plen;
   _delay_ms(10);
   enc28j60Init(mymac);//��������� ������� ������������� ENC28J60
   _delay_ms(20);
   enc28j60PhyWrite(PHLCON,0x476);//����������� ����� ������ ����������� ENC28J60:���� ���������
   //���������� ������ ���������,������-�������� � �����
   _delay_ms(20);
   init_ip_arp(mymac,myip);//��������� ������� ����������� MAC- ������ � IP-������ �� �����������
   //��������
      asm("sei"); // ��������� ���������� ����������
   
   while(1)
   {
	  // PORTD|=(1<<PD4);
	   if (usart_data == '\r'){ // ��� ������� ������������� ��������� 'r' - �������� ������, ���������� ����� ������
	   led_0_status(1, 1000); //�������� ��������� 
	   buf[UDP_DATA_P]=data_cmd[0]; //� ����� �������� enc28j60 ��������� �������� �� ������� ������ usart
	   buf[UDP_DATA_P+1]=data_cmd[1];
	   buf[UDP_DATA_P+2]=data_cmd[2];
	   send_udp_prepare(buf,1200,dip,1201,dstmac); // ���������� ���� ��������� ( �� ����� � ���� ������)
	   send_udp_transmit(buf,3);
	   ClearTotalBuff(); // ��� �������
	   }
	   
	 //  _delay_ms(500);
	   plen = enc28j60PacketReceive(BUFFER_SIZE, buf);//��������� ������� ������ ������� �� ENC28J60
	   if(plen==0)// ���� �������� ������ ���
	   {
		   continue;//�� ������� �� ������ ����� while
	   }
	   if(eth_type_is_arp_and_my_ip(buf,plen))// ���� ���������� ������ ARP-������,������������ ���
	   {
		   make_arp_answer_from_request(buf);//�� ������������� � ��������� ARP-�����
		   continue;// ������� �� ������ ����� while
	   }
	   if(eth_type_is_ip_and_my_ip(buf,plen)==0)// ���� ���������� ������ ������ ip-������� � ���������� ��
	   //���
	   {
		   continue;// ������� �� ������ ����� while
	   }
	   
	   if(buf[IP_PROTO_P]==IP_PROTO_ICMP_V&&buf[ICMP_TYPE_P]==ICMP_TYPE_ECHOREQUEST_V)//���� �
	   //���������� ip-������ ��������� ������ "����"
	   {
		   make_echo_reply_from_request(buf,plen);// �������� ����� "����"
		   continue;//������� �� ������ ����� while
	   }

	   if(buf[IP_PROTO_P]==IP_PROTO_UDP_V&&buf[UDP_DST_PORT_H_P]==4&&buf[UDP_DST_PORT_L_P]==0xb0)//���� � �������� ������ ��� ��������� � ��������� IP UDP � ������������ ���
	   {
		   led_1_status(1, 1000);
		   Ether_Transmit(buf[UDP_DATA_P]);
		   Ether_Transmit(buf[UDP_DATA_P+1]);
		   Ether_Transmit(buf[UDP_DATA_P+2]);
		   Ether_Transmit('\r');
		  /* if(buf[UDP_DATA_P]==49)//���� �������� ����� 1
		   {
			   PORTD=0x80;//������ ��������� D7
			   char data[11]="Diod_is_on";//������ �������� ����� ����� Diod_is_on
			   make_udp_reply_from_request(buf,data,11,myport);// ��������� ����� UDP
			   continue;
		   }
		   if(buf[UDP_DATA_P]==48)// ���� �������� ����� 0
		   {
			   PORTD=PORTD&(~(1<<7));// �������� ��������� D7
			   char data[11]="Diod_is_off";//������ �������� ����� ����� Diod_is_off
			   make_udp_reply_from_request(buf,data,11,myport);// ��������� ����� UDP
			   continue;
		   }*/
		   continue;
	   }
	   
	   return (0);
   }
   }

